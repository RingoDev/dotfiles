syntax on
set number
set smartindent
set autoindent
set nospell

colorscheme default 

"Install vim-plug if not already installed
"
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
endif

"calling vim plugins

call plug#begin('~/.vimplugins')
"Plug 'vim-pandoc/vim-pandoc'
"Plug 'vim-pandoc/vim-pandoc-syntax'
"Plug 'cespare/vim-toml'
"Plug 'fatih/vim-go'
"Plug 'tpope/vim-surround'
"Plug 'frazrepo/vim-rainbow'
"Plug 'jiangmiao/auto-pairs'
"Plug 'airblade/vim-gitgutter'
"Plug 'PProvost/vim-ps1'
call plug#end()

let g:pandoc#modules#disabled = [ "spell" ]
