set -o vi

#aliases

unalias -a

alias c=clear
alias ll="ls -la"
alias vi=vim
alias ls='ls --color=auto'
#alias dir='dir --color=auto'
#alias vdir='vdir --color=auto'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias vpnc="sudo protonvpn c -f"
alias vpndc="sudo protonvpn disconnect"

#opens my rwx BeginnerBoost notes if i am in a tmux session
alias notes="tmux splitw -l 10 -v -b -c ~/repos/gitlab/ringodev/bboost/notes/ vim README.md -c :$"

#opens README.md if exists in directory
alias o="vim README.md"

#git aliases
alias gp="git push origin master"
alias gaa="git add -A"
alias gc="git commit -m 'working commit'"

#spotify shortcut
alias music="tmux new -s spotify ncspot"

#opens the linuxCLI book
alias pdf="mupdf -I ~/Documents/linuxCommandline.pdf"

#rwx build script
alias build="~/repos/gitlab/ringodev/beginnerboost/build"

#custom prompt

export WECHALLUSER="ringodev"

export WECHALLTOKEN="4150F-D7FCE-51E29-7D88C-DBAEE-27D6A"

export PS1='${debian_chroot:+($debian_chroot)}\[\033[01;33m\]\w\[\033[00m\]\$ '

export PATH=$HOME/repos/gitlab/ringodev/dotfiles/scripts:$PATH
